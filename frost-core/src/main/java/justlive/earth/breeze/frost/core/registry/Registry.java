package justlive.earth.breeze.frost.core.registry;

/**
 * 注册接口
 * 
 * @author wubo
 *
 */
public interface Registry {

  /**
   * 注册
   */
  void register();

  /**
   * 注销
   */
  void unregister();
}
